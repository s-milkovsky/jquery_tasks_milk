// 10-я строка. Чтобы при нажатии на <Р> менялся его цвет.
$('#menu > p').addClass('active');

/*15-я строка DEFAULT Найти этот список по тому, 
что в нем ссылка, и в этом списке подсветить последний <li> */
$('[href="#abc"]').parent().addClass('active');

// 20-я строка Всем <li> кроме последнего добавить класс "active"
$('#find').parent().find('li:not(:last-child)').addClass('active');


/* 66-я строка Найти Label с текстом 'I was born'
Чтобы при каждом нажатии менялся цвет.*/
$('label:contains("I was born")').addClass('active');
$('label:contains("I was born")').click(function(){
    $(this).toggleClass('changeColor');
});

/*87-я строка  Найти первый input [type=checkbox] который по дефолту 
checked, прародитель которого имеет label*/

$('label').parents('.search').find('input[type="checkbox"]:checked').addClass('checked');

// 89-я Выбрать последний input в этом div-е
$('fieldset > .search:last-child input:last-child').addClass('checked');

/* 108-я строка Найти див который содержит <p> с тектом Nunc, который находится 
после h5 который первый из списка h5 в этом диве */
$('div h5:first + p:contains("Nunc")').addClass('active');

// 111-я Найти последний заголовок в диве, который лежит рядом c заголовком h1. При наведении пусть меняется фон.
$('h1 + div :header:last').addClass('active');
$('h1 + div :header:last').mouseover(function(){
    $(this).toggleClass('changeColor');
})

// 128-я строка Выбрать второй <p>. и делайте с ним все, что хотите. теперь он Ваш!!
$('p.left').parent().find('p:last').addClass('active');
